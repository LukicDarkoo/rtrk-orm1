#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <err.h>

#define DEFAULT_BUFLEN 1024
#define DEFAULT_PORT 80

char response[] =  "HTTP/1.1 200 OK\r\n"
"Server: LDServer\r\n"
"Last-Modified: Tue, 1 Dec 2015 19:15:56 GMT\r\n"
"Vary: Accept-Encoding\r\n"
"Content-Type: text/plain\r\n\r\n"
"By LukicDarkoo :)\r\n";

int main() { 	
	struct sockaddr_in server_address;
	struct sockaddr_in client_address;
	int client_socket;
	int server_socket = socket(AF_INET, SOCK_STREAM, 0);

	// Check if connection is established
	if (server_socket == -1) {
		err(1, "Can't open socket");
	}
	
	// Define socket & bind
	server_address.sin_family = AF_INET;
	server_address.sin_addr.s_addr = INADDR_ANY;
	server_address.sin_port = htons(DEFAULT_PORT);

	if (bind(server_socket, (struct sockaddr *)&server_address, sizeof(server_address)) == -1) {
		err(1, "Can't bind socket");
	}

	// Listen for connections
	listen(server_socket, 3);


	// Connect to client
	puts("Listening...");
	int client_address_size = sizeof(client_address);
	client_socket = accept(server_socket, (struct sockaddr *)&client_address, (socklen_t *)&client_address_size);
	if (client_socket < 0) {
		err(1, "Can't accept connection");
	}

	write(client_socket, response, sizeof(response) - 1);

	// Read message from client
	puts("Reading answer...");
	char message[DEFAULT_BUFLEN];
	int message_size = 0;
	while (1) {
		message_size = recv(client_socket, message, DEFAULT_BUFLEN, 0);
		message[message_size] = 0;
		if (message_size > 0) {
			printf("%s", message);
			break;
		}
	}
	
	puts("Closing client and socket...");
	close(client_socket);
	close(server_socket);
	
	
	return 0;
}
