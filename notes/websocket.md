# Websocket
Kako bi ostvario live komunijaciju 


## Handshaking

To prove that the handshake was received, the server has to take two
 pieces of information and combine them to form a response.  The first
 piece of information comes from the |Sec-WebSocket-Key| header field
 in the client handshake:

      Sec-WebSocket-Key: dGhlIHNhbXBsZSBub25jZQ==

 For this header field, the server has to take the value (as present
 in the header field, e.g., the base64-encoded [RFC4648] version minus
 any leading and trailing whitespace) and concatenate this with the
 Globally Unique Identifier (GUID, [RFC4122]) "258EAFA5-E914-47DA-
 95CA-C5AB0DC85B11" in string form, which is unlikely to be used by
 network endpoints that do not understand the WebSocket Protocol.  A
 SHA-1 hash (160 bits) [FIPS.180-3], base64-encoded (see Section 4 of
 [RFC4648]), of this concatenation is then returned in the server's
 handshake.
[Page 7]

dGhlIHNhbXBsZSBub25jZQ==
s3pPLMBiTxaQ9kYGzzhZRbK+xOo=

## Frames
[http://stackoverflow.com/questions/8125507/how-can-i-send-and-receive-websocket-messages-on-the-server-side]

	0                   1                   2                   3
  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 +-+-+-+-+-------+-+-------------+-------------------------------+
 |F|R|R|R| opcode|M| Payload len |    Extended payload length    |
 |I|S|S|S|  (4)  |A|     (7)     |             (16/64)           |
 |N|V|V|V|       |S|             |   (if payload len==126/127)   |
 | |1|2|3|       |K|             |                               |
 +-+-+-+-+-------+-+-------------+ - - - - - - - - - - - - - - - +
 |     Extended payload length continued, if payload len == 127  |
 + - - - - - - - - - - - - - - - +-------------------------------+
 |                               |Masking-key, if MASK set to 1  |
 +-------------------------------+-------------------------------+
 | Masking-key (continued)       |          Payload Data         |
 +-------------------------------- - - - - - - - - - - - - - - - +
 :                     Payload Data continued ...                :
 + - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +
 |                     Payload Data continued ...                |
 +---------------------------------------------------------------+
page 28


## Sha1 biblioteka za enkodovanje u C
[http://oauth.googlecode.com/svn/code/c/liboauth/src/sha1.c]

## Chromium errors
### Received unexpected continuation frame.
Potrebno uraditi Pings and Pongs
### Invalid frame header
Header nije dobro formatiran
### Error during WebSocket handshake: Incorrect 'Sec-WebSocket-Accept' header value
Obrati paznju na Sec-WebSocket-Accept, trebalo bi da je = base64(sha1(Sec-WebSocket-Key))
### Error in connection establishment: net::ERR_CONNECTION_REFUSED
Server vjerovato nije pokrenut

## Base64 encode
[https://en.wikipedia.org/wiki/Base64]

## Protocol
[https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API/Writing_WebSocket_servers]
[https://tools.ietf.org/html/rfc6455](1.2.  Protocol Overview)

## JavaScript
[https://developer.mozilla.org/en-US/docs/Web/API/WebSocket](WebSocket in JavaScript for client)
