# HTTP Server

**Server je potrebno pokretati sa sudo komandom kako bi se pristupilo portu 80**
[http://stackoverflow.com/questions/5871885/user-mode-permission-denied-to-bind-to-socket]

## Izvorni kod
http_server.c


## Reference
[https://tools.ietf.org/html/rfc7230](2.1.  Client/Server Messaging)

## Parsiranje HTMLa
Da bi browser znao da treba da parsira HTML potrebno je promijeniti Content-Type u text/html. Kompletno definisana linija sa naznacenim HTML parsiranjem i podrskom za UTF8 encoding bi izgledala ```Content-Type: text/html; charset=UTF-8```
