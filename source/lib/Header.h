#ifndef Header_h
#define Header_h

#include <string.h>
#include "Defines.h"

typedef struct {
	char SecWebSocketAccept[100];
} HeaderHandshakeResponse;

typedef struct {
	char SecWebSocketKey[25];
} HeaderHandshakeRequest;


extern void HeaderMakeHandshakeResponse(char *responseHeader, HeaderHandshakeResponse *inputHeader);
extern void HeaderParseHandshakeRequest(char *requestHeader, HeaderHandshakeRequest *outputHeader);

void readValue(char *intputHeaderStart, char *value);

#endif
