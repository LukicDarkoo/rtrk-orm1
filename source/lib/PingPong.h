#ifndef PingPong_h
#define PingPong_h

#include <stdlib.h>
#include <sys/socket.h>
#include <unistd.h>

extern void PingPongExecute(int clientSocket, char *frame, char *message);

#endif
