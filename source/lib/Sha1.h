// Based on: http://oauth.googlecode.com/svn/code/c/liboauth/src/sha1.c
#ifndef Sha1_h
#define Sha1_h

#include <stdint.h>
#include <string.h>


#define HASH_LENGTH 20
#define BLOCK_LENGTH 64
#define SHA1_K0  0x5a827999
#define SHA1_K20 0x6ed9eba1
#define SHA1_K40 0x8f1bbcdc
#define SHA1_K60 0xca62c1d6

typedef struct sha1nfo {
	uint32_t buffer[BLOCK_LENGTH/4];
	uint32_t state[HASH_LENGTH/4];
	uint32_t byteCount;
	uint8_t bufferOffset;
	uint8_t keyBuffer[BLOCK_LENGTH];
	uint8_t innerHash[HASH_LENGTH];
} sha1nfo;


extern void Sha1Init(sha1nfo *s);
extern void Sha1Write(sha1nfo *s, const char *data, size_t len);
extern uint8_t* Sha1Result(sha1nfo *s);

void sha1Writebyte(sha1nfo *s, uint8_t data);

#endif
