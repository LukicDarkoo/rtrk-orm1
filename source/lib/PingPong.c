#include "PingPong.h"

void PingPongExecute(int clientSocket, char *frame, char *message) {
	short pongFrameLenght = frame[1] & 127;
	char *pongFrame = (char *)malloc(pongFrameLenght + 2);
	pongFrame[0] = 138;
	pongFrame[1] = pongFrameLenght;

	short i;
	for (i = 0; i < pongFrameLenght; i++) {
		pongFrame[2 + i] = message[i];
	}

	write(clientSocket, pongFrame, pongFrameLenght + 2);
	free(pongFrame);
}
