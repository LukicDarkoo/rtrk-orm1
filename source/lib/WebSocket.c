#include "WebSocket.h"

int WebsocketOpen(char *address) {
	// Find port
	int port = atoi(strstr(&address[4], ":") + 1);

	// Init client number
	clientNumber = 0;

	// Make socket
	serverSocket = socket(AF_INET, SOCK_STREAM, 0);

	// Check if connection is established
	if (serverSocket == -1) {
		return WEBSOCKET_ERROR;
	}

	// Define socket & bind
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_addr.s_addr = INADDR_ANY;
	serverAddress.sin_port = htons(port);

	if (bind(serverSocket, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) == -1) {
		return WEBSOCKET_ERROR;
	}

	return WEBSOCKET_SUCCESS;
}

int WebsocketSend(char *message) {
	int frameSize = strlen(message) + 2;
	uint8_t *frame = (uint8_t *)malloc(frameSize);
	memset(frame, 0, frameSize);
	FrameMake(frame, message);

	int i;
	for (i = 0; i < clientNumber; i++) {
		write(clientSockets[i], frame, frameSize);
	}
	free(frame);
	return WEBSOCKET_SUCCESS;
}

int WebsocketClose() {
	int i;
	for (i = 0; i < clientNumber; i++) {
		close(clientSockets[i]);
	}
	close(serverSocket);
	return WEBSOCKET_SUCCESS;
}

int WebsocketReceive(char *message, int index) {
	uint8_t frame[WEBSOCKET_DEFAULT_BUFLEN];

	recv(clientSockets[index], frame, WEBSOCKET_DEFAULT_BUFLEN, 0);

	// Check opcode
	switch (frame[0] & 0x0f) {
		case WEBSOCKET_OPCODE_PING:
			PingPongExecute(clientSockets[index], frame, message);
			return WEBSOCKET_ERROR;
		break;

		case WEBSOCKET_OPCODE_TEXT: case WEBSOCKET_OPCODE_BINARY:
			return FrameParse(message, frame);
		break;

		case WEBSOCKET_OPCODE_CLOSE:
			WebsocketClose();
			return WEBSOCKET_ERROR;
		break;
	}

	return WEBSOCKET_ERROR;
}

int WebsocketListen() {
	if (clientNumber < WEBSOCKET_MAX_CLIENTS) {
		int clientAddressSize = sizeof(clientAddress);

		// Listen for connections
		listen(serverSocket, 1);

		// Accept connection
		clientSockets[clientNumber] = accept(serverSocket, (struct sockaddr *)&clientAddress, (socklen_t *)&clientAddressSize);
		if (clientSockets[clientNumber] < 0) {
			return WEBSOCKET_ERROR;
		}

		// Do handshake
		HandshakeExecute(clientSockets[clientNumber]);

		// Increment number of clients
		clientNumber++;
	}

	return WEBSOCKET_SUCCESS;
}
