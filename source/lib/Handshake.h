#ifndef Handshake_h
#define Handshake_h

#include <sys/types.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include "Header.h"
#include "Sha1.h"

extern int HandshakeExecute(int clientSocket);

void makeSecWebsocketAccept(char *hex, char *buf);

// Reference: https://en.wikipedia.org/wiki/Base64
void sha1ToBase64(char *in, char *out);

#endif
