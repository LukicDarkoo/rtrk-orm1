#include "Sha1.h"

void Sha1Init(sha1nfo *s) {
	s->state[0] = 0x67452301;
	s->state[1] = 0xefcdab89;
	s->state[2] = 0x98badcfe;
	s->state[3] = 0x10325476;
	s->state[4] = 0xc3d2e1f0;
	s->byteCount = 0;
	s->bufferOffset = 0;
}

uint32_t sha1Rol32(uint32_t number, uint8_t bits) {
	return ((number << bits) | (number >> (32-bits)));
}

void sha1HashBlock(sha1nfo *s) {
	uint8_t i;
	uint32_t a,b,c,d,e,t;

	a=s->state[0];
	b=s->state[1];
	c=s->state[2];
	d=s->state[3];
	e=s->state[4];
	for (i=0; i<80; i++) {
		if (i>=16) {
			t = s->buffer[(i+13)&15] ^ s->buffer[(i+8)&15] ^ s->buffer[(i+2)&15] ^ s->buffer[i&15];
			s->buffer[i&15] = sha1Rol32(t,1);
		}
		if (i<20) {
			t = (d ^ (b & (c ^ d))) + SHA1_K0;
		} else if (i<40) {
			t = (b ^ c ^ d) + SHA1_K20;
		} else if (i<60) {
			t = ((b & c) | (d & (b | c))) + SHA1_K40;
		} else {
			t = (b ^ c ^ d) + SHA1_K60;
		}
		t+=sha1Rol32(a,5) + e + s->buffer[i&15];
		e=d;
		d=c;
		c=sha1Rol32(b,30);
		b=a;
		a=t;
	}
	s->state[0] += a;
	s->state[1] += b;
	s->state[2] += c;
	s->state[3] += d;
	s->state[4] += e;
}

void sha1AddUncounted(sha1nfo *s, uint8_t data) {
	uint8_t * const b = (uint8_t*) s->buffer;
	b[s->bufferOffset ^ 3] = data;
	s->bufferOffset++;
	if (s->bufferOffset == BLOCK_LENGTH) {
		sha1HashBlock(s);
		s->bufferOffset = 0;
	}
}

void sha1Writebyte(sha1nfo *s, uint8_t data) {
	++s->byteCount;
	sha1AddUncounted(s, data);
}

void Sha1Write(sha1nfo *s, const char *data, size_t len) {
	for (;len--;) sha1Writebyte(s, (uint8_t) *data++);
}

void sha1_pad(sha1nfo *s) {
	sha1AddUncounted(s, 0x80);
	while (s->bufferOffset != 56) sha1AddUncounted(s, 0x00);

	// Append length in the last 8 bytes
	sha1AddUncounted(s, 0); // We're only using 32 bit lengths
	sha1AddUncounted(s, 0); // But SHA-1 supports 64 bit lengths
	sha1AddUncounted(s, 0); // So zero pad the top bits
	sha1AddUncounted(s, s->byteCount >> 29); // Shifting to multiply by 8
	sha1AddUncounted(s, s->byteCount >> 21); // as SHA-1 supports bitstreams as well as
	sha1AddUncounted(s, s->byteCount >> 13); // byte.
	sha1AddUncounted(s, s->byteCount >> 5);
	sha1AddUncounted(s, s->byteCount << 3);
}

uint8_t* Sha1Result(sha1nfo *s) {
	// Pad to complete the last block
	sha1_pad(s);

	int i;
	for (i=0; i<5; i++) {
		s->state[i]=
			  (((s->state[i])<<24)& 0xff000000)
			| (((s->state[i])<<8) & 0x00ff0000)
			| (((s->state[i])>>8) & 0x0000ff00)
			| (((s->state[i])>>24)& 0x000000ff);
	}

	// Return pointer to hash (20 characters)
	return (uint8_t*) s->state;
}
