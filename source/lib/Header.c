#include "Header.h"

void HeaderMakeHandshakeResponse(char *output, HeaderHandshakeResponse *inputHeader) {
	strcpy(output, "HTTP/1.1 101 Switching Protocols\r\n");
	strcat(output, "Upgrade: websocket\r\n");
	strcat(output, "Connection: Upgrade\r\n");

	strcat(output, "Sec-WebSocket-Accept: ");
	strcat(output, inputHeader->SecWebSocketAccept);
	strcat(output, "\r\n\r\n");
}

void HeaderParseHandshakeRequest(char *inputHeader, HeaderHandshakeRequest *outputHeader) {
	char* search = "Sec-WebSocket-Key: ";
	char* begin = strstr(inputHeader, search);
	readValue(begin + strlen(search), outputHeader->SecWebSocketKey);
}

void readValue(char *intputHeaderStart, char *value) {
	int maxIterations = 200; // Simple protection
	int i = 0;

	while (1) {
		if (intputHeaderStart[i] != '\n' &&
				intputHeaderStart[i] != '\r') {
			value[i] = intputHeaderStart[i];
		} else {
			value[i] = 0;
			break;
		}

		i++;
		if (maxIterations <= i) {
			value[i] = 0;
			break;
		}
	}
}
