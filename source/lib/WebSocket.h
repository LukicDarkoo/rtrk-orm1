#ifndef WebSocket_h
#define WebSocket_h

#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include "Handshake.h"
#include "Frame.h"
#include "PingPong.h"
#include "Defines.h"

int clientSockets[WEBSOCKET_MAX_CLIENTS];
int clientNumber;
int serverSocket;
struct sockaddr_in serverAddress;
struct sockaddr_in clientAddress;

extern int WebsocketOpen(char *address);
extern int WebsocketListen(); 
extern int WebsocketSend(char *data);
extern int WebsocketReceive(char *message, int index);
extern int WebsocketClose();


#endif
