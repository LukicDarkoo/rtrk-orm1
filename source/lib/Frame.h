#ifndef Frame_h
#define Frame_h

#include <string.h>
#include <stdint.h>
#include <stdlib.h>

extern void FrameMake(uint8_t *frame, char *message);
extern int FrameParse(char *message, uint8_t *frame);

#endif
