#ifndef Defines_h
#define Defines_h

#define WEBSOCKET_OPCODE_CONTINUATION 0x0
#define WEBSOCKET_OPCODE_TEXT 0x1
#define WEBSOCKET_OPCODE_BINARY 0x2
#define WEBSOCKET_OPCODE_CLOSE 0x8
#define WEBSOCKET_OPCODE_PING 0x9
#define WEBSOCKET_OPCODE_PONG 0xA

#define WEBSOCKET_ERROR -1
#define WEBSOCKET_ERROR_HANDSHAKE -2
#define WEBSOCKET_SUCCESS 1

#define WEBSOCKET_DEFAULT_BUFLEN 2048
#define WEBSOCKET_MAX_CLIENTS 10

#endif
