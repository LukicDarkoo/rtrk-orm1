#include "Handshake.h"

int HandshakeExecute(int clientSocket) {
	char message[WEBSOCKET_DEFAULT_BUFLEN];
	int messageSize = 0;
	HeaderHandshakeRequest structRequestHeader;
	HeaderHandshakeResponse structResponseHeader;

	// Receive header from client
	while (1) {
		messageSize = recv(clientSocket, message, WEBSOCKET_DEFAULT_BUFLEN, 0);
		message[messageSize] = 0;
		if (messageSize > 0) {
			break;
		}
	}

	// Send server handshake header
	// Parse header to get Sec-WebSocket-Key
	HeaderParseHandshakeRequest(message, &structRequestHeader);

	// Generate Sec-WebSocket-Accept
	char secWebsocketAccept[100];
	makeSecWebsocketAccept(secWebsocketAccept, structRequestHeader.SecWebSocketKey);

	// Make response header
	char handshakeResponse[1000];
	strcpy(structResponseHeader.SecWebSocketAccept, secWebsocketAccept);
	HeaderMakeHandshakeResponse(handshakeResponse, &structResponseHeader);


	write(clientSocket, handshakeResponse, sizeof(handshakeResponse) - 1);

	// TODO: Handle error
	return WEBSOCKET_SUCCESS;
}

void sha1ToBase64(char *in, char *out) {
	char codes[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

	int i = 0;
	int b = 0;
	int outIndex = 0;
	while (i < 20) {
		b = (in[i] & 0xFC) >> 2;
		out[outIndex++] = codes[b];
		b = (in[i] & 0x03) << 4;
		if (i + 1 < 20)      {
        b |= (in[i + 1] & 0xF0) >> 4;
        out[outIndex++] = codes[b];
        b = (in[i + 1] & 0x0F) << 2;
        if (i + 2 < 20)  {
            b |= (in[i + 2] & 0xC0) >> 6;
            out[outIndex++] = codes[b];
            b = in[i + 2] & 0x3F;
            out[outIndex++] = codes[b];
        } else  {
            out[outIndex++] = codes[b];
            out[outIndex++] = '=';
        }
    } else      {
        out[outIndex++] = codes[b];
        out[outIndex++] = '=';
				out[outIndex++] = '=';
    }

		i+=3;
	}
}

void makeSecWebsocketAccept(char *hex, char *buf) {
	uint8_t *results = (uint8_t *) malloc(20 * sizeof(uint8_t));

	strcat(buf, "258EAFA5-E914-47DA-95CA-C5AB0DC85B11");
	int n = strlen(buf);

	sha1nfo s;
	Sha1Init(&s);
	Sha1Write(&s, buf, strlen(buf));
	results = Sha1Result(&s);
	sha1ToBase64(results, hex);
	hex[28] = 0;
}
