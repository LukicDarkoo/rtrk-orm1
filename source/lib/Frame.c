#include "Frame.h"

void FrameMake(uint8_t *frame, char *message) {
	// TODO: Make support for more types of frames
	int frameIndex = 0;
	int messageLength;
	int i;

	frame[0] = 0b10000001;

	messageLength = strlen(message);
	if (messageLength <= 125) {
		frame[1] = messageLength;
		frameIndex = 2;
	}
	else if (messageLength >= 126 && messageLength <= 65535) {
		frame[1] = 126;
		frame[2] = (messageLength >> 8) & 255;
		frame[3] = messageLength & 255;
		frameIndex = 4;
	}
	else {
		frame[1] = 127;
		frame[2] = ((uint64_t)messageLength >> 56) & 255;
		frame[3] = ((uint64_t)messageLength >> 48) & 255;
		frame[4] = ((uint64_t)messageLength >> 40) & 255;
		frame[5] = ((uint64_t)messageLength >> 32) & 255;
		frame[6] = (messageLength >> 24) & 255;
		frame[7] = (messageLength >> 16) & 255;
		frame[8] = (messageLength >> 8) & 255;
		frame[9] = messageLength & 255;
		frameIndex = 10;
	}

	for (i = 0; i < messageLength; i++) {
		frame[frameIndex + i] = message[i];
	}
}

int FrameParse(char *message, uint8_t *frame) {
	uint8_t secondByte = frame[1];
	int length = secondByte & 127;
	int realDataLength = length;
	int indexFirstMask = 2;
	int indexFirstDataByte;
	int i, j;

	if (length == 126) {
		realDataLength = frame[2] << 8;
		realDataLength = realDataLength & ((short)frame[3]);
		indexFirstMask = 4;
	}
	else if (length == 127) {
		realDataLength = (uint64_t)realDataLength << 56;
		realDataLength = realDataLength & ((uint64_t)frame[3] << 48);
		realDataLength = realDataLength & ((uint64_t)frame[4] << 40);
		realDataLength = realDataLength & ((uint64_t)frame[5] << 32);
		realDataLength = realDataLength & ((uint32_t)frame[6] << 24);
		realDataLength = realDataLength & ((uint32_t)frame[7] << 16);
		realDataLength = realDataLength & ((uint32_t)frame[8] << 8);
		realDataLength = realDataLength & (short)frame[9];
		indexFirstMask = 10;
	}

	// Parse with mask
	uint8_t masks[4];
	for (i = 0; i < 4; i++) {
		masks[i] = frame[indexFirstMask + i];
	}

	indexFirstDataByte = indexFirstMask + 4;
	for (i = indexFirstDataByte, j = 0; j < realDataLength; i++, j++) {
		message[j] = frame[i] ^ masks[j % 4];
	}
	message[j] = 0;

	return realDataLength;
}
