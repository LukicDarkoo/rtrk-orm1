#include <pthread.h>
#include <stdio.h>
#include "lib/WebSocket.h"

pthread_t receiverThread[10];
int lastClientIndex = -1;

void *receiver(void *clientIndex) {
	while (1) {
		char message[WEBSOCKET_DEFAULT_BUFLEN];
		while (WebsocketReceive(message, *((int *)clientIndex) ) <= 0);
		WebsocketSend(message);
	}
}

void *listener(void *td) {
	while (1) {
		if (WebsocketListen() == WEBSOCKET_SUCCESS) {
			lastClientIndex++;
			pthread_create(&(receiverThread[lastClientIndex]), NULL, receiver, (void *)&lastClientIndex);
		}
	}
}


int main() {
	int i;
	pthread_t listenerThread;

	// Open websocket
	WebsocketOpen("ws://127.0.0.1:9099");

	// Create thread
	pthread_create(&listenerThread, NULL, listener, NULL);

	// Pause
	scanf("Press any key to finish... %d", &i);

	// Close all threads
	pthread_exit(&listenerThread);
	for (i = 0; i < lastClientIndex + 1; i++) {
		pthread_exit(&receiverThread[i]);
	}

	// Close websocket
	WebsocketClose();

	return 0;
}
