# WebSocket server implementation in C

## Compile & run server
Run ```make```

## Run client
Just open ```index.html```

## Debug
All messages made by client will be visible in web browser's console ```Ctrl + Shift + C```