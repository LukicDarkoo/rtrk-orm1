SRC = $(wildcard source/*.c)
SRC += $(wildcard source/lib/*.c)
OUT = server
LIB = -lm -pthread

all:
	@echo Compiling...
	@gcc $(SRC) -o $(OUT) $(LIB)
	@echo Running...
	@./$(OUT)

clean:
	@echo Cleaning...
	@rm $(OUT)
